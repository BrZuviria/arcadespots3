import enum
import click
import json
import copy
import numpy as np
from SALib.sample import saltelli
from SALib.analyze import sobol
import pandas as pd 


@click.group()
def cli():
    pass

@click.command()
@click.option('--n', help='sample size', default=128)
@click.option('--ns', help='n_simulations', default=5)
@click.argument('TEMPLATE', type=click.File('r'))
@click.argument('PROBLEM', type=click.File('r'))
@click.argument('OUTPUT')
def sample(template, problem, output, n, ns):
    """
    generates a set of parameters
    """

    template = json.load(template)[0]
    problem = json.load(problem)
    variable_names = problem['names']
    param_values = saltelli.sample(problem, n)
    output_params = []

    for i, X in enumerate(param_values):
        template_copy = copy.deepcopy(template)
        for j, name in enumerate(variable_names):
            template_copy['global_parameters'][name] = X[j]
        template_copy['metaparameters']['simulations'] = ns 
        template_copy['metaparameters']['seeds'] = np.arange(ns).tolist() 
        template_copy['metaparameters']['outfile'] = output + f'_{i:06d}'
        output_params.append(template_copy)

    with open(output + '.txt', 'w') as f:

        f.write(','.join(variable_names) + '\n')
        np.savetxt(f, param_values, delimiter=',')

    with open(output + '.json', 'w') as f:

        json.dump(output_params, f, indent=4)


@click.command()
@click.option('--season', type=int, default=0)
@click.argument('INPUT', type=click.File('r'))
@click.argument('PROBLEM', type=click.File('r'))
@click.argument('OUTPUT', type=click.File('w'))
def analyze(input, problem, output, season):
    """
    analyzes a sobol sensitivity test
    """
    data = pd.read_csv(
        input, names=[
            'crop', 'time', 'exposition', 'infective',
            'infective_ac', 'alive', 'inoculum', 'coinfected',
            'sim'
        ]
    )
    problem = json.load(problem)
    data_max_infected = data.groupby(['crop', 'sim']).max()
    Y = np.zeros(data['sim'].max(), dtype='float64')
    for i in range(data['sim'].max()):
        Y = data_max_infected.loc[season, i]['infective_ac']

    sa = sobol.analyze(problem, Y)
    json.dump(sa, output, indent=4, sort_keys=True)


cli.add_command(sample)
cli.add_command(analyze)


if __name__ == '__main__':

    cli()
